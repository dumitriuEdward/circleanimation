//
//  ViewController.swift
//  CircleAnimation
//
//  Created by Edward Dumitriu on 6/24/20.
//  Copyright © 2020 Edward Dumitriu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var circle: CircleAnimation!
    private let leftSide = false
    
    @IBOutlet weak var showHideBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
            self.initCercleAnimatio()
        }
    }
    
    @IBAction func showHide(_ sender: Any) {
        showHideBtn.setTitle(showHideBtn.currentTitle == "SHOW" ? "HIDE" : "SHOW" , for: .normal)
        self.circle.toggle()
    }
}

extension ViewController: CircleAnimationDelegate {
    fileprivate func getOriginAnimationBtnPosition() -> CGPoint {
        guard let cercle = circle else { return CGPoint(x: 0, y: 0) }
        
        let radius  = CGFloat(cercle.distanceFromOriginRadius)
        let x = leftSide ? (radius - CGFloat(cercle.heightBtn)) : (UIScreen.main.bounds.width - radius)
        let y = UIScreen.main.bounds.height - radius - 1.5 * CGFloat(cercle.heightBtn)
        
        return CGPoint(x: x + CGFloat(leftSide ? -30 : 30), y: y + 30)
        
    }
    
    fileprivate func getStartPositionAnimationBtnPosition() -> CGPoint {
        let diagonale  = CGFloat(circle.distanceFromOriginRadius * 2)
        let x = leftSide ? 0 : UIScreen.main.bounds.width
        let y = UIScreen.main.bounds.height - diagonale
        return CGPoint(x: x, y: y)
    }
    
    func resetBtnAnimationPosition() {
        guard let cercle = circle else { return }
        let origin = getOriginAnimationBtnPosition()
        cercle.originCercle = origin
        cercle.startPositionAnimation = getStartPositionAnimationBtnPosition()
        cercle.setBtnPosition()
    }
    
    fileprivate func initCercleAnimatio() {
        
        circle = CircleAnimation(parentView: self.view)
        circle.delegate = self
        circle.heightBtn = 50
        circle.distanceFromOriginRadius = 85
        circle.distanceAngle = 50
        circle.autohide = false
        
        //represents the center of the circle in which the buttons will be displayed
        let origin = getOriginAnimationBtnPosition()
        
        circle.originCercle = origin
        circle.startPositionAnimation = getStartPositionAnimationBtnPosition()
        
        if leftSide {
            circle.firstBtnPositionAtDegree = 30
            circle.showClockDirection = false
        } else {
            circle.firstBtnPositionAtDegree = 150
        }
        
        circle.initAfterMakeSetUp(icons: [UIImage(systemName: "pencil.circle")!, UIImage(systemName: "calendar.circle")!, UIImage(systemName: "globe")!], originCercle: origin)
        
        //Show btns if is first content opened
        //self.cercleBtns.activateView(forceShow: true)
    }
    
    func buttonPressed(index: Int, button: UIButton) {
        switch index {
        case 0: print("Button 0 was pressed")
        case 1: print("Button 1 was pressed")
        case 2: print("Button 2 was pressed")
        default: break
        }
    }
    
    func animationIsCompleated(isShowUp: Bool) {
        print("animationIsCompleated ans is show up", isShowUp)
    }
    
    func beginAnimation(willShowUp: Bool) {
        print("Begin animation")
    }
}
