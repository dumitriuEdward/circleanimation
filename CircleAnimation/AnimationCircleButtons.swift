//
//  AnimationCercleButtons.swift
//  CircleAnimation
//
//  Created by Edward Dumitriu on 6/24/20.
//  Copyright © 2020 Edward Dumitriu. All rights reserved.
//

import Foundation
import UIKit

protocol CircleAnimationDelegate: class {
    func animationIsCompleated(isShowUp: Bool)
    func beginAnimation(willShowUp: Bool)
    func buttonPressed(index: Int, button: UIButton)
}

class CircleAnimation {
    
    class OriginalPositionMoreButton {
        private(set) var button: UIButton
        private(set) var originalPosition: CGPoint
        
        init(btn: UIButton, originalPos: CGPoint) {
            button = btn
            originalPosition = originalPos
        }
    }
    
    //Public properties
    private(set) var buttons: [OriginalPositionMoreButton] = []
    var delegate: CircleAnimationDelegate?
    var autohide = true
    var distanceAngle: Double = 50.0

    lazy var distanceFromOriginRadius: Double = smallDevice ? 60.0 : 80.0 //distanta intre elemente

    var heightBtn: Double {
        set { height = smallDevice ? newValue-15 : newValue }
        get { let h: Double = height ?? (smallDevice ? 50-15 : 50); height = h; return h }
    }
    var timeAutohide = 1.5
    var originCercle: CGPoint
    var startPositionAnimation: CGPoint!
    var showClockDirection = true
    var firstBtnPositionAtDegree: Double = 120.0
    
    //Private
    private let smallDevice = false//Device.current.diagonal < 4.7
    private var height: Double?
    private var timer = Timer()
    private var timerIsActive =  false
    private var animationTime = 0.25
    private var isCercleOpen = false
    private var animationIsInProgress = false
    private var view: UIView
    
    //Is recomandate to init in viewDidAppear
    init(parentView: UIView) {
        view = parentView
        self.originCercle =  CGPoint(x: UIScreen.main.bounds.width, y: UIScreen.main.bounds.height - UIScreen.main.bounds.height*0.21)
        if startPositionAnimation == nil {
            startPositionAnimation =  CGPoint(x: UIScreen.main.bounds.width, y: UIScreen.main.bounds.height - UIScreen.main.bounds.height*0.21)}
        if height == nil { heightBtn = 50 }
    }
    
    func initAfterMakeSetUp(icons: [UIImage], originCercle: CGPoint) {
        self.originCercle = originCercle
        if startPositionAnimation == nil {
            startPositionAnimation =  CGPoint(x: UIScreen.main.bounds.width, y: UIScreen.main.bounds.height - UIScreen.main.bounds.height*0.21)}
        if height == nil { heightBtn = 50 }
        initAllBtnsForAnimation(elements: icons)
    }
    
    //Public Methods
    func toggle(forceShow: Bool = false) {
        if forceShow {
            animateShowHideControlBottomView(showView: true)
            return
        }
        
        if animationIsInProgress { print("animation is not finished"); return }
        animateShowHideControlBottomView(showView: !isCercleOpen)
    }
    
    @objc func resetTimerForAutohide() {
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: timeAutohide, target: self, selector: #selector(startAutoHideControlBottomView), userInfo: nil, repeats: false)
        timerIsActive = true
    }
    
    @objc func pressBtn(selectedBtn: UIButton) {
        resetTimerForAutohide()
        delegate?.buttonPressed(index: selectedBtn.tag, button: selectedBtn)
    }
    
    func stopTimer() { timer.invalidate() }
    
    //Private Methods
    var btns: [UIButton] = []
    private func initAllBtnsForAnimation(elements: [UIImage]) {
        //init originalPosition , go to start position and show btns
        elements.forEachEnumerated {index, img in
            let btn = UIButton(x: 0, y: 0, w: CGFloat(self.heightBtn), h: CGFloat(self.heightBtn))
            btn.addTarget(self, action: #selector(self.pressBtn), for: .touchUpInside)
            btn.tag = index
            btn.setBackgroundImage(img, for: .normal)
            self.view.addSubview(btn)
            self.btns.append(btn)
        }
       
        setBtnPosition()
    }
    
    func setBtnPosition() {
        self.buttons = []
        btns.forEachEnumerated { index, btn in
            var transform = Double(index)*self.distanceAngle
            if !self.showClockDirection { transform = transform * -1 }
            let x = self.distanceFromOriginRadius*cos((self.firstBtnPositionAtDegree + transform) * (Double.pi / 180))
            let y = self.distanceFromOriginRadius*sin((self.firstBtnPositionAtDegree + transform) * (Double.pi / 180))
            let origin = CGPoint(x: Double(self.originCercle.x) + x, y: Double(self.originCercle.y) + y)
            self.buttons.append(OriginalPositionMoreButton(btn: btn, originalPos: origin))
        }
        
        buttons.forEach { btn in
            btn.button.frame.origin = self.startPositionAnimation
            btn.button.isHidden = false
            btn.button.alpha = 0
        }
    }
    
    private func runBtnsAnimation(_ show: Bool) {
        var index = show ? 0 : buttons.count - 1
        let animationBtnsCount = show ? buttons.count : -1
        Timer.scheduledTimer(withTimeInterval: 0.08, repeats: true) { timerStartAnim in
            
            UIView.animate(withDuration: 0.35) { [weak self] in
                guard let `self` = self else { return }
                self.buttons[index].button.frame.origin = show ? self.buttons[index].originalPosition : self.startPositionAnimation
                self.buttons[index].button.alpha = show ? 1 : 0
            }
            if show { index += 1 } else { index -= 1 }
            if index == animationBtnsCount {
                timerStartAnim.invalidate()
                DispatchQueue.main.asyncAfter(deadline: .now()+0.35) {
                    self.animationIsInProgress = false
                    self.timerIsActive = false
                    self.timer.invalidate()
                    self.delegate?.animationIsCompleated(isShowUp: show)
                    if show && self.autohide {
                        self.timer = Timer.scheduledTimer(timeInterval: self.timeAutohide, target: self, selector: #selector(self.startAutoHideControlBottomView), userInfo: nil, repeats: false)
                        self.timerIsActive = true
                    }
                }
            }
        }
    }
    
    //change status
    private func animateShowHideControlBottomView(showView: Bool) {
        delegate?.beginAnimation(willShowUp: showView)
        isCercleOpen = showView
        if timerIsActive && !showView { timerIsActive = false; timer.invalidate() }
        animationIsInProgress = true
        runBtnsAnimation(showView)
    }
    
    //Observables
    @objc private func startAutoHideControlBottomView() {
        toggle()
    }
}

extension Array {
    
    public func forEachEnumerated(_ body: @escaping (_ offset: Int, _ element: Element) -> Void) {
        enumerated().forEach(body)
    }
    
    public func get(at index: Int) -> Element? {
        guard index >= 0 && index < count else { return nil }
        return self[index]
    }
}

extension UIView {
    public convenience init(x: CGFloat, y: CGFloat, w: CGFloat, h: CGFloat) {
        self.init(frame: CGRect(x: x, y: y, width: w, height: h))
    }
}
